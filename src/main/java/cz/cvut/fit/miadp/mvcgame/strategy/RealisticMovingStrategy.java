package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

public class RealisticMovingStrategy implements MovingStrategy {

    @Override
    public void updatePosition(AbsMissile missile) {
        double initAngle = missile.getInitAngle( );
        int initVelocity = missile.getInitVelocity( );
        long time = missile.getAge( ) / 150;

        int dX = ( int )(( initVelocity * time * Math.cos( initAngle ))/2 );
        int dY = ( int )(( initVelocity * time * Math.sin( initAngle ) + ( 0.5 * MvcGameConfig.GRAVITY * time * time))/2 );

        missile.move( new Vector( dX, dY ) );
    }

    @Override
    public String getName() {
        return "Realistic";
    }

}
