package cz.cvut.fit.miadp.mvcgame.abstractFactory;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.*;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;

public class GameObjectFactoryA implements GameObjectFactory {

    private IGameModel model;

    public GameObjectFactoryA(IGameModel gameModel) {
        this.model = gameModel;
    }

    @Override
    public AbsCannon createCannon() {
        return new CannonA( new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y), this );
    }

    @Override
    public AbsMissile createMissile( AbsCannon cannon ) {
        return new MissileA( new Position(cannon.getPosition().getX(), cannon.getPosition().getY()), cannon.getAngle(), cannon.getPower(), this.model.getMovingStrategy() );
    }

    @Override
    public AbsMissile createMissile( Position position, double angle, int power ) {
        return new MissileA( new Position(position.getX(), position.getY()), angle, power, new SimpleMovingStrategy());
    }

    @Override
    public AbsCollision createCollision( Position position ) {
        return new CollisionA(new Position(position.getX(), position.getY()));
    }

    @Override
    public AbsGameInfo createGameInfo() {
        return new GameInfoA();
    }

    @Override
    public AbsSight createSight(Position position, SightPattern sightPattern) {
        return new Sight( new Position(position.getX(), position.getY()), sightPattern );
    }
}
