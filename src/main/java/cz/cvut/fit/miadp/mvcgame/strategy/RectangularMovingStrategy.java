package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

public class RectangularMovingStrategy implements MovingStrategy {

    @Override
    public void updatePosition(AbsMissile missile) {
        int initVelocity = missile.getInitVelocity();
        long time = missile.getAge();

        if (time >= 1)
            missile.move(new Vector( 0 , 5));
        else
            missile.move(new Vector( (int)Math.sqrt( MvcGameConfig.MOVE_STEP * initVelocity / 4 ) , 0));
    }

    @Override
    public String getName() {
        return "Rectangular";
    }
}
