package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class JavaFX implements IGameGraphicsImplementator {

    private GraphicsContext gc;

    public JavaFX( GraphicsContext gc ) {
        this.gc = gc;
    }

    @Override
    public void drawImage(String path, Position position) {
        this.gc.drawImage( new Image( path ), position.getX(), position.getY() );
    }

    @Override
    public void drawText(String text, Position position) {
        this.gc.fillText( text, position.getX(), position.getY() );
    }

    @Override
    public void drawLine(Position startPosition, Position endPosition) {
        this.gc.strokeLine( startPosition.getX(), startPosition.getY(), endPosition.getX(), endPosition.getY() );
    }

    @Override
    public void drawBackground() {
        this.gc.drawImage( new Image( "images/background.png" ), 0, 0 );
    }

    @Override
    public void clear() {
        this.gc.clearRect(0, 0, MvcGameConfig.MAX_X, MvcGameConfig.MAX_Y);
    }
}
