package cz.cvut.fit.miadp.mvcgame.model;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;

public class Vector {

    private int vecX = 0;
    private int vecY = 0;

    public Vector( ) {
    }

    public Vector( int vecX, int vecY ) {
        this.vecX = vecX;
        this.vecY = vecY;
    }

    public int getX( ) {
        return this.vecX;
    }

    public int getY( ) {
        return this.vecY;
    }

    public void setX( int vecX ) {
        this.vecX = vecX;
    }

    public void setY( int vecY ) {
        this.vecY = vecY;
    }


}
