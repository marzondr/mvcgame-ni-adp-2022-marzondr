package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsSight;

public class SimpleMovingStrategy implements MovingStrategy {

    @Override
    public void updatePosition( AbsMissile missile ) {
        double initAngle = missile.getInitAngle( );
        int initVelocity = missile.getInitVelocity( );
        long time = missile.getAge( ) / 100;

        int X = ( int )( initVelocity * Math.cos( initAngle ) );
        int Y = ( int )( initVelocity * Math.sin( initAngle ) );

        missile.move( new Vector( X, Y ) );
    }

    @Override
    public String getName() {
        return "Simple";
    }
}
