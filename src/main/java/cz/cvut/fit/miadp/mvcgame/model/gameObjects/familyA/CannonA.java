package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.state.DoubleShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.DynamicShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.SingleShootingMode;

import java.util.ArrayList;
import java.util.List;

public class CannonA extends AbsCannon {

    private GameObjectFactory gameObjectFactory;
    private List<AbsMissile> shootingBatch;

    public CannonA(Position position, GameObjectFactory gameObjectFactory){
        this.position = position;
        this.gameObjectFactory = gameObjectFactory;

        this.angle = MvcGameConfig.INIT_ANGLE;
        this.power = MvcGameConfig.INIT_POWER;

        this.shootingBatch = new ArrayList<>();
        this.shootingMode = new SingleShootingMode();
    }

    @Override
    public List<AbsMissile> shoot( ) {
        this.shootingBatch.clear( );
        this.shootingMode.shoot( this );
        return this.shootingBatch;
    }

    @Override
    public void primitiveShoot() {
        this.shootingBatch.add(
                this.gameObjectFactory.createMissile( this )
        );
    }

    @Override
    public void moveUp( ) {
        if (this.position.getY() <= -60)
            this.position.setY( MvcGameConfig.MAX_Y );
        else
            this.move( new Vector( 0, -1 * MvcGameConfig.MOVE_STEP ) );
    }

    @Override
    public void moveDown( ) {
        if (this.position.getY() >= MvcGameConfig.MAX_Y + 20)
            this.position.setY( 0 );
        else
            this.move( new Vector( 0, MvcGameConfig.MOVE_STEP ) );
    }

    @Override
    public void moveLeft() {

    }

    @Override
    public void moveRight() {

    }

    @Override
    public void aimUp() {
        if (this.angle > -0.98)
            this.angle -= MvcGameConfig.STEP_ANGLE;
    }

    @Override
    public void aimDown() {
        if (this.angle < 0.98)
            this.angle += MvcGameConfig.STEP_ANGLE;
    }

    @Override
    public void powerUp() {
        if (this.power < 3 * MvcGameConfig.INIT_POWER)
            this.power += MvcGameConfig.STEP_POWER;
    }

    @Override
    public void powerDown() {
        if (this.power > MvcGameConfig.INIT_POWER / 2)
            this.power -= MvcGameConfig.STEP_POWER;
    }

    @Override
    public void increaseMissiles() {
        if (this.missilesAllowed < 10)
            this.missilesAllowed += 1;
    }

    @Override
    public void decreaseMissiles() {
        if (this.missilesAllowed > 1)
            this.missilesAllowed -= 1;
    }

    @Override
    public List<AbsMissile> getShootingBatch() {
        return shootingBatch;
    }
}
