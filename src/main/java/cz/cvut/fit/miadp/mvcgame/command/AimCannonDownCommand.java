package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

public class AimCannonDownCommand extends AbsCommand {

    IGameModel model;

    public AimCannonDownCommand(IGameModel model) {
        this.model = model;
    }

    @Override
    protected void execute() {
        model.aimCannonDown();
    }
}
