package cz.cvut.fit.miadp.mvcgame.view;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.controller.GameController;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;
import cz.cvut.fit.miadp.mvcgame.visitor.GameRender;

public class GameView implements IObserver {

    private GameController controller;
    private IGameModel model ;
    private IGameGraphics gr;
    private GameRender render;

    public GameView( IGameModel model ){
        this.model = model;
        this.model.registerObserver( this );
        this.controller = new GameController( model );
        this.gr = null;
        this.render = new GameRender();
    }

    public GameController getController( ) {
        return this.controller;
    }

    public void render( ) {
        this.gr.clear();
        this.model.getGameInfo().acceptVisitor( this.render );
        for ( GameObject go : this.model.getGameObjects( ) ) {
            go.acceptVisitor( this.render );
        }
    }

    public void setGraphicContext( IGameGraphics gr ) {
        this.gr = gr;
        this.render.setGraphicContext( gr );
        this.update( );
    }

    @Override
    public void update( ) {
        this.render( );
    }
}
