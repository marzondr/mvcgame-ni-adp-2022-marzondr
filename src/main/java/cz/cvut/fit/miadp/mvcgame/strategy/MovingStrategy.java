package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsSight;

public interface MovingStrategy {

    void updatePosition(AbsMissile missile);
    public String getName( );
}
