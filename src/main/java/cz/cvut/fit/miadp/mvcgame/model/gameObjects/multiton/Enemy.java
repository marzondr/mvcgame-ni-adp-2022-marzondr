package cz.cvut.fit.miadp.mvcgame.model.gameObjects.multiton;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Enemy extends AbsEnemy {

    private static Map<Integer, Enemy> enemies;
    private final int num;

    static {
        enemies = new ConcurrentHashMap<>();

        for (int i = 0; i < MvcGameConfig.ENEMIES_NUM; i++)
            enemies.put(i, new Enemy(i));
    }

    private static Position nextPosition() {
        return new Position(new Random().nextInt() % 300 + 800, new Random().nextInt() % 200 + 300);
    }

    private static void reappear(int num) {
        getInstance(num).alive = true;
        getInstance(num).position = nextPosition();
    }

    private Enemy(int num) {
        this.num = num;
        this.alive = true;
        this.position = new Position(new Random().nextInt() % 300 + 800, new Random().nextInt() % 200 + 300);
    }

    public static Enemy getInstance( int num ) {
        return enemies.get(num);
    }

    public static Enemy getNthAliveInstance( int num ) {
        int n = 0;
        for (Enemy enemy: enemies.values()) {
            if (enemy.isAlive()) {
                n++;
                if (n == num)
                    return enemy;
            }
        }
        return enemies.get(0);
    }

    public static Collection<Enemy> getInstances() {

        Collection<Enemy> aliveCollection = new ArrayList<>();

        for (Enemy enemy: enemies.values()) {
            if (enemy.isAlive())
                aliveCollection.add(enemy);
        }

        return aliveCollection;
    }

    public static void checkNumOfAlive() {
        if ((numAlive() < MvcGameConfig.ENEMIES_NUM && numAlive() > MvcGameConfig.ENEMIES_NUM - 7 && new Random().nextInt() % 100 == 0)
            || numAlive() <= MvcGameConfig.ENEMIES_NUM - 7) {
            for (int i = 0; i < MvcGameConfig.ENEMIES_NUM; i++) {
                if (!getInstance(i).isAlive()) {
                    reappear(i);
                    break;
                }
            }
        }
    }

    public static int numAlive() {
        int sumAlive = 0;

        for (Enemy enemy: enemies.values()) {
            if (enemy.isAlive()) sumAlive += 1;
        }

        return sumAlive;
    }

    public boolean isAlive() {
        return alive;
    }

    public int getNum() {
        return num;
    }
}
