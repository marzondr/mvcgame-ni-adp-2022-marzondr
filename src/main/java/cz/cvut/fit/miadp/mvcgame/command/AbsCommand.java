package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.memento.Caretaker;

public abstract class AbsCommand {

    Object memento;

    protected abstract void execute();

    public void doExecute() {
        this.memento = Caretaker.getInstance( ).createMemento();
        this.execute();
    }

    public void unExecute() {
        Caretaker.getInstance( ).setMemento( this.memento );
    }
}
