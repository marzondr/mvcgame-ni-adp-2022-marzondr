package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.state.DoubleShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.DynamicShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.SingleShootingMode;

import java.util.ArrayList;
import java.util.List;

public class CannonB extends AbsCannon {

    private GameObjectFactory gameObjectFactory;
    private List<AbsMissile> shootingBatch;

    public CannonB(Position position, GameObjectFactory gameObjectFactory){
        this.position = position;
        this.gameObjectFactory = gameObjectFactory;

        this.angle = MvcGameConfig.INIT_ANGLE;
        this.power = MvcGameConfig.INIT_POWER * 2;

        this.shootingBatch = new ArrayList<>();
        this.shootingMode = new SingleShootingMode();
    }

    @Override
    public List<AbsMissile> shoot( ) {
        this.shootingBatch.clear( );
        this.shootingMode.shoot( this );
        return this.shootingBatch;
    }

    @Override
    public void primitiveShoot() {
        this.shootingBatch.add(
                this.gameObjectFactory.createMissile(
                        this
                )
        );
    }

    public void moveUp( ) {
        if (this.position.getY() > -60)
            this.move( new Vector( 0, -1 * MvcGameConfig.MOVE_STEP ) );
    }

    public void moveDown( ) {
        if (this.position.getY() < MvcGameConfig.MAX_Y + 20)
            this.move( new Vector( 0, MvcGameConfig.MOVE_STEP ) );
    }

    public void moveLeft( ) {
        if (this.position.getX() > -20)
            this.move( new Vector( -1 * MvcGameConfig.MOVE_STEP, 0 ) );
    }

    public void moveRight( ) {
        if (this.position.getX() < MvcGameConfig.MAX_X + 20)
            this.move( new Vector( MvcGameConfig.MOVE_STEP, 0 ) );
    }

    @Override
    public void aimUp() {
        angle += MvcGameConfig.STEP_ANGLE;
    }

    @Override
    public void aimDown() {
        angle -= MvcGameConfig.STEP_ANGLE;
    }

    @Override
    public void powerUp() {
        this.power += MvcGameConfig.STEP_POWER;
    }

    @Override
    public void powerDown() {
        if (this.power > MvcGameConfig.INIT_POWER / 2)
            this.power -= MvcGameConfig.STEP_POWER;
    }

    @Override
    public List<AbsMissile> getShootingBatch() {
        return shootingBatch;
    }

    @Override
    public void decreaseMissiles() {
        this.missilesAllowed -= 1;
    }

    @Override
    public void increaseMissiles() {
        this.missilesAllowed += 1;
    }

}
