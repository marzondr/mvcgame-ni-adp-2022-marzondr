package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class SoundEffects implements Visitor {

    Clip clip;
    AudioInputStream audioInputStream;

    @Override
    public void visitCannon(AbsCannon cannon) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        audioInputStream = AudioSystem.getAudioInputStream(new File("src/main/resources/sounds/cannon.wav").getAbsoluteFile());

        clip = AudioSystem.getClip();
        clip.open(audioInputStream);
        clip.start();
    }

    @Override
    public void visitMissile(AbsMissile missile) {

    }

    @Override
    public void visitEnemy(AbsEnemy enemy) {

    }

    @Override
    public void visitCollision(AbsCollision collision) throws UnsupportedAudioFileException, IOException, LineUnavailableException {
        audioInputStream = AudioSystem.getAudioInputStream(new File("src/main/resources/sounds/collision.wav").getAbsoluteFile());

        clip = AudioSystem.getClip();
        clip.open(audioInputStream);
        clip.start();
    }

    @Override
    public void visitGameInfo(AbsGameInfo gameInfo) {

    }

    @Override
    public void visitSight(AbsSight sight) {

    }
}
