package cz.cvut.fit.miadp.mvcgame.state;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;

public class DoubleShootingMode implements ShootingMode {

    @Override
    public void shoot( AbsCannon cannon ) {
        cannon.setAngle( cannon.getAngle() + 0.15 );
        cannon.primitiveShoot( );
        cannon.setAngle( cannon.getAngle() - 0.3 );
        cannon.primitiveShoot( );
        cannon.setAngle( cannon.getAngle() + 0.15 );
    }

    @Override
    public String getName( ) {
        return "Double";
    }
}
