package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

public class MissilesDecreasedCommand extends AbsCommand {

    IGameModel model;

    public MissilesDecreasedCommand(IGameModel model) {
        this.model = model;
    }

    @Override
    protected void execute() {
        model.cannonMissilesDecrease();
    }
}
