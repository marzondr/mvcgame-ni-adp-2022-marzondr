package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;

public class GameRender implements Visitor {

    private IGameGraphics gr;

    public void setGraphicContext( IGameGraphics gr ) {
        this.gr = gr;
    }

    public void visitGameInfo(AbsGameInfo info) {
        this.gr.drawText(info.getText(), new Position(10, 20));
    }

    @Override
    public void visitCannon(AbsCannon cannon) {
        this.gr.drawImage( "images/cannon.png", cannon.getPosition() );
    }

    @Override
    public void visitMissile(AbsMissile missile) {
        this.gr.drawImage( "images/missile.png", missile.getPosition());
    }

    @Override
    public void visitEnemy(AbsEnemy enemy) {
        this.gr.drawImage( "images/enemy1.png", enemy.getPosition() );
    }

    @Override
    public void visitCollision(AbsCollision collision) {
        this.gr.drawImage( "images/collision.png", collision.getPosition() );
    }

    @Override
    public void visitSight(AbsSight sight) {
        this.gr.drawImage( "images/" + sight.getSightPatternImage(), sight.getPosition() );
    }
}
