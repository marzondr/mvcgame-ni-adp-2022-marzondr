package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

public class MoveCannonUpCommand extends AbsCommand {

    IGameModel model;

    public MoveCannonUpCommand(IGameModel model) {
        this.model = model;
    }

    @Override
    protected void execute() {
        model.moveCannonUp();
    }
}
