package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

public class ChangeMovingStrategyCommand extends AbsCommand {

    IGameModel model;

    public ChangeMovingStrategyCommand(IGameModel model) {
        this.model = model;
    }

    @Override
    protected void execute() {
        model.changeMovingStrategy();
    }
}
