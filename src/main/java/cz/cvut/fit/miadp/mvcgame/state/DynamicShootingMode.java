package cz.cvut.fit.miadp.mvcgame.state;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;

public class DynamicShootingMode implements ShootingMode {

    @Override
    public void shoot(AbsCannon cannon) {
        int initPower = cannon.getPower();

        for (int i = 0; i < cannon.getMissilesAllowed(); i++) {
            cannon.primitiveShoot();
            cannon.setPower(cannon.getPower() - 10 );
        }

        cannon.setPower( initPower );
    }

    @Override
    public String getName() {
        return "Dynamic";
    }
}
