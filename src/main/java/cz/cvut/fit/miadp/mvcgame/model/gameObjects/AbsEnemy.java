package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.visitor.Visitor;

public class AbsEnemy extends GameObject {

    protected boolean alive;

    public void die() {
        this.alive = false;
    }

    @Override
    public void acceptVisitor(Visitor visitor) {
        visitor.visitEnemy(this);
    }
}
