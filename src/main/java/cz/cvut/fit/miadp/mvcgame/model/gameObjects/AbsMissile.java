package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.visitor.Visitor;

public abstract class AbsMissile extends LifetimeLimitedGameObject {

    private double initAngle;
    private int initVelocity;
    private int hits;

    protected AbsMissile(Position position, double initAngle, int initVelocity) {
        super(position);
        this.initAngle = initAngle;
        this.initVelocity = initVelocity;
    }

    public void hit() {
        this.hits++;
    }

    public int getHits() {
        return this.hits;
    }


    @Override
    public void acceptVisitor( Visitor visitor ) {
        visitor.visitMissile( this );
    }

    public int getInitVelocity() {
        return this.initVelocity/10;
    }

    public double getInitAngle() {
        return this.initAngle;
    }

    public abstract void move();
}
