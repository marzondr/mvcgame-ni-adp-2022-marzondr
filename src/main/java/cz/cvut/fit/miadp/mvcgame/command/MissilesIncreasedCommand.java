package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

public class MissilesIncreasedCommand extends AbsCommand {

    IGameModel model;

    public MissilesIncreasedCommand(IGameModel model) {
        this.model = model;
    }

    @Override
    protected void execute() {
        model.cannonMissilesIncrease();
    }
}
