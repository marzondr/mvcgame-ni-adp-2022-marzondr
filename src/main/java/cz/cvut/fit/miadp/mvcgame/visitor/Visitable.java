package cz.cvut.fit.miadp.mvcgame.visitor;

public interface Visitable {

    public void acceptVisitor( Visitor visitor );
}

