package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.model.Position;

public interface IGameGraphicsImplementator {

    public void drawImage( String path, Position position );
    public void drawText( String text, Position position );
    public void drawLine( Position startPosition, Position endPosition );
    public void drawBackground();
    public void clear();
}
