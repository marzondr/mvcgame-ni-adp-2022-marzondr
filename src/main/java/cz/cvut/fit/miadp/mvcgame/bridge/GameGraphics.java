package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.model.Position;

public class GameGraphics implements IGameGraphics {

    IGameGraphicsImplementator implementator;

    public GameGraphics( IGameGraphicsImplementator implementator ) {
        this.implementator = implementator;
    }


    @Override
    public void drawImage(String path, Position position) {
        this.implementator.drawImage(path, position);
    }

    @Override
    public void drawText(String text, Position position) {
        this.implementator.drawText(text, position);
    }

    @Override
    public void drawRectangle(Position startPosition, Position endPosition) {
        this.implementator.drawLine(startPosition, endPosition);
        this.implementator.drawLine(startPosition, new Position(startPosition.getX(),  startPosition.getY()+endPosition.getX()-startPosition.getX()));
        this.implementator.drawLine(new Position(startPosition.getX(),  startPosition.getY()+endPosition.getX()-startPosition.getX()), new Position(endPosition.getX(),  startPosition.getY()+endPosition.getX()-startPosition.getX()));
        this.implementator.drawLine(endPosition, new Position(startPosition.getY(),  startPosition.getY()+endPosition.getX()-startPosition.getX()));
    }

    @Override
    public void drawBackground() {
        this.implementator.drawBackground();
    }

    @Override
    public void clear() {
        this.implementator.clear();
    }
}
