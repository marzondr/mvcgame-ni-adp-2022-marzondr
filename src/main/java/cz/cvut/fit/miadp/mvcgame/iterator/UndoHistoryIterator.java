package cz.cvut.fit.miadp.mvcgame.iterator;

import cz.cvut.fit.miadp.mvcgame.command.AbsCommand;
import cz.cvut.fit.miadp.mvcgame.command.RedoCommand;
import cz.cvut.fit.miadp.mvcgame.command.UndoCommand;

import java.util.Stack;

public class UndoHistoryIterator implements HistoryIterator{

    private Stack<AbsCommand> doExecutedCommands = new Stack<>();
    private Stack<AbsCommand> backedCommands = new Stack<>();

    public UndoHistoryIterator( ) {
    }

    @Override
    public void back() {
        if (!doExecutedCommands.isEmpty()) {
            AbsCommand cmd = doExecutedCommands.pop();
            cmd.unExecute();
            backedCommands.add(cmd);
        }
    }

    @Override
    public void forward() {
        if (!backedCommands.isEmpty()) {
            AbsCommand cmd = backedCommands.pop();
            cmd.unExecute();
            doExecutedCommands.add(cmd);
        }
    }

    @Override
    public void add( AbsCommand cmd ) {
        if (cmd instanceof UndoCommand || cmd instanceof RedoCommand) return;
        backedCommands.clear();
        doExecutedCommands.add( cmd );
    }
}
