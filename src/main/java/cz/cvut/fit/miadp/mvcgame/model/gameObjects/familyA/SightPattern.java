package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

public class SightPattern {

    String imageName;

    public SightPattern(String imageName) {
        this.imageName = imageName;
    }

    public String getImageName() {
        return imageName;
    }
}
