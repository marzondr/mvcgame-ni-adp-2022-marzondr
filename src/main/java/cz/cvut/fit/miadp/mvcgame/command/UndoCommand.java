package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

public class UndoCommand extends AbsCommand {

    IGameModel model;

    public UndoCommand(IGameModel model) {
        this.model = model;
    }

    @Override
    protected void execute() {
        model.undoLastCommand();
    }
}
