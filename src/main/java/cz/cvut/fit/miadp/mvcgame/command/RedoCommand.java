package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

public class RedoCommand extends AbsCommand {

    IGameModel model;

    public RedoCommand(IGameModel model) {
        this.model = model;
    }

    @Override
    protected void execute() {
        model.redoLastCommand();
    }
}
