package cz.cvut.fit.miadp.mvcgame.memento;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

public class Caretaker {

    private IGameModel model;

    private static class SingletonHolder {
        private static final Caretaker INSTANCE = new Caretaker( );
    }

    public static Caretaker getInstance( ) {
        return SingletonHolder.INSTANCE;
    }

    public void setModel( IGameModel model ){
        this.model = model;
    }

    public Object createMemento(){
        if ( this.model != null ){
            Object m = this.model.createMemento( );
            return m;
        }
        return null;
    }

    public void setMemento( Object memento ){
        if( this.model != null ) {
            this.model.setMemento( memento );
        }
    }

}
