package cz.cvut.fit.miadp.mvcgame.proxy;

import cz.cvut.fit.miadp.mvcgame.command.AbsCommand;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.multiton.Enemy;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.strategy.MovingStrategy;

import java.util.List;

public interface IGameModel {

    public void update( );
    public void cannonShoot( );
    public void moveCannonUp( );
    public void moveCannonDown( );
    public void moveCannonLeft( );
    public void moveCannonRight( );
    public void changeFactoryFamily();
    public void changeMovingStrategy();
    public void registerObserver( IObserver obs );
    public void unregisterObserver( IObserver obs );
    public void notifyObservers( );
    public int getScore();
    public List<GameObject> getGameObjects();
    public List<Enemy> getEnemies();
    public List<AbsMissile> getMissiles();
    public List<AbsCollision> getCollisions();
    public MovingStrategy getMovingStrategy();
    public AbsCannon getCannon();
    public AbsGameInfo getGameInfo();
    public void aimCannonUp();
    public void aimCannonDown();
    public void cannonPowerUp();
    public void cannonPowerDown();
    public void toggleCannonShootingMode();
    public void cannonMissilesIncrease();
    public void cannonMissilesDecrease();
    public void setMemento(Object memento);
    public Object createMemento();

    public void registerCommand(AbsCommand cmd);
    public void redoLastCommand();
    public void undoLastCommand();
}
