package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsGameInfo;
import cz.cvut.fit.miadp.mvcgame.state.DynamicShootingMode;

public class GameInfoA extends AbsGameInfo {

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(GameModel model) {
        text = "Skóre: " + model.getScore() + "\n" +
                "Pohybová strategie: " + model.getMovingStrategy().getName() + "\n" +
                "Mód střílení: " + model.getCannon().getShootingModeName();

        if (model.getCannon().getShootingMode() instanceof DynamicShootingMode)
            text += " <" + model.getCannon().getMissilesAllowed() + ">";

        text += "\n\n" +
                "Úhel míření: " + model.getCannon().getRealAngle() + "°\n" +
                "Síla střílení: " + model.getCannon().getPower() + "%\n\n" +

                "Aktivní rakety: " + model.getMissiles().size() + "\n" +
                "Aktivnní nepřátelé: " + model.getEnemies().size() + "\n" +
                "Aktivní kolize: " + model.getCollisions().size();
    }
}
