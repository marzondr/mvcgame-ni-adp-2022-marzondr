package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.visitor.Visitor;

public abstract class AbsSight extends GameObject {

    @Override
    public void acceptVisitor( Visitor visitor ) {
        visitor.visitSight( this );
    }

    public abstract String getSightPatternImage();
}
