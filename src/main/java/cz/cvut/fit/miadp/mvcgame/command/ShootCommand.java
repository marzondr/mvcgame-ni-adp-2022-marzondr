package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

public class ShootCommand extends AbsCommand {

    IGameModel model;

    public ShootCommand(IGameModel model) {
        this.model = model;
    }

    @Override
    protected void execute() {
        model.cannonShoot();
    }
}
