package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

public interface Visitor {

    public void visitCannon(AbsCannon cannon) throws LineUnavailableException, UnsupportedAudioFileException, IOException;
    public void visitMissile(AbsMissile missile);
    public void visitEnemy(AbsEnemy enemy);
    public void visitCollision(AbsCollision collision) throws UnsupportedAudioFileException, IOException, LineUnavailableException;
    public void visitGameInfo(AbsGameInfo gameInfo);
    public void visitSight(AbsSight sight);
}
