package cz.cvut.fit.miadp.mvcgame.state;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;

public interface ShootingMode {

    public void shoot(AbsCannon cannon );
    public String getName( );
}