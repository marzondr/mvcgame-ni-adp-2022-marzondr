package cz.cvut.fit.miadp.mvcgame.config;

public class MvcGameConfig {
    public static final int MAX_X = 1280;
    public static final int MAX_Y = 720;
    public static final int MOVE_STEP = 10;
    public static final int CANNON_POS_X = 50;
    public static final int CANNON_POS_Y = MAX_Y / 2;
    public static final int ENEMIES_NUM = 10;

    public static final double GRAVITY = 9.8;
    public static final double INIT_ANGLE = 0;
    public static final int INIT_POWER = 100;
    public static final double STEP_ANGLE = Math.PI / 200;
    public static final int STEP_POWER = 1;
    public static final long MAX_MIS_AGE = 10000;
    public static final int AIM_LENGHT = 7;
}