package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

public class GoniometricMovingStrategy implements MovingStrategy {

    @Override
    public void updatePosition(AbsMissile missile) {
        double initAngle = missile.getInitAngle( );
        int initVelocity = missile.getInitVelocity( );

        int X = (int)(initVelocity/2.5);
        int Y = (int)( initVelocity * Math.cos( initAngle ) * Math.cos( missile.getPosition().getX()/10. ));

        missile.move(new Vector( X, Y));
    }

    @Override
    public String getName() {
        return "Goniometric";
    }
}
