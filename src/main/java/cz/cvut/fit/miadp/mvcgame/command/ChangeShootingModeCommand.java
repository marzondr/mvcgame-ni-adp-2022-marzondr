package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

public class ChangeShootingModeCommand extends AbsCommand {

    IGameModel model;

    public ChangeShootingModeCommand(IGameModel model) {
        this.model = model;
    }

    @Override
    protected void execute() {
        model.toggleCannonShootingMode();
    }
}
