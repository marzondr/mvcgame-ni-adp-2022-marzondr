package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.state.DoubleShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.DynamicShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.ShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.SingleShootingMode;
import cz.cvut.fit.miadp.mvcgame.visitor.Visitor;

import java.util.List;

public abstract class AbsCannon extends GameObject {

    protected double angle = 0.;
    protected int power = 0;
    protected int missilesAllowed = 0;
    protected ShootingMode shootingMode;
    protected static ShootingMode SINGLE_SHOOTING_MODE = new SingleShootingMode( );
    protected static ShootingMode DOUBLE_SHOOTING_MODE = new DoubleShootingMode( );
    protected static ShootingMode DYNAMIC_SHOOTING_MODE = new DynamicShootingMode( );

    public abstract List<AbsMissile> shoot();
    public abstract void primitiveShoot();

    public abstract void moveUp( );
    public abstract void moveDown( );
    public abstract void moveLeft( );
    public abstract void moveRight( );
    public abstract void aimUp();
    public abstract void aimDown();
    public abstract void powerUp();
    public abstract void powerDown();

    @Override
    public void acceptVisitor( Visitor visitor )  {
        try {
            visitor.visitCannon(this);
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    public void toggleShootingMode( ) {
        if( this.shootingMode instanceof SingleShootingMode ){
            this.shootingMode = DOUBLE_SHOOTING_MODE;
        }
        else if( this.shootingMode instanceof DoubleShootingMode ){
            this.missilesAllowed = 1;
            this.shootingMode = DYNAMIC_SHOOTING_MODE;
        }
        else if( this.shootingMode instanceof DynamicShootingMode){
            this.shootingMode = SINGLE_SHOOTING_MODE;
        }
    }

    public void setSingleShootingMode() {
        this.shootingMode = SINGLE_SHOOTING_MODE;
    }

    public void setDoubleShootingMode() {
        this.shootingMode = DOUBLE_SHOOTING_MODE;
    }

    public void setDynamicShootingMode() {
        this.shootingMode = DYNAMIC_SHOOTING_MODE;
    }

    public int getMissilesAllowed() {
        return this.missilesAllowed;
    }

    public void setMissilesAllowed(int missilesAllowed) { this.missilesAllowed = missilesAllowed; }

    public double getAngle() {
        return this.angle;
    }

    // převedeme na stupně
    public int getRealAngle()  {
        return (int)((angle * 60)%360) * -1;
    }

    public void setAngle(double angle) { this.angle = angle; }

    public int getPower() {
        return power;
    }

    public void setPower(int power) { this.power = power; }

    public ShootingMode getShootingMode() {
        return shootingMode;
    }
    public String getShootingModeName() {
        return shootingMode.getName();
    }

    public abstract List<AbsMissile> getShootingBatch();

    public abstract void decreaseMissiles();

    public abstract void increaseMissiles();
}
