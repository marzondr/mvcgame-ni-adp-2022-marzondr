package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.visitor.Visitor;

public abstract class AbsCollision extends LifetimeLimitedGameObject {

    protected AbsCollision(Position position) {
        super(position);
    }

    @Override
    public void acceptVisitor( Visitor visitor ) {
        try {
            visitor.visitCollision( this );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }
}
