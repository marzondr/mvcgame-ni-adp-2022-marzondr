package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB;

import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsGameInfo;

public class GameInfoB extends AbsGameInfo {

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(GameModel model) {
        text = "Skóre: " + model.getScore();
    }
}
