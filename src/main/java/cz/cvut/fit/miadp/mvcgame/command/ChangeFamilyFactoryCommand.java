package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

public class ChangeFamilyFactoryCommand extends AbsCommand {

    IGameModel model;

    public ChangeFamilyFactoryCommand(IGameModel model) {
        this.model = model;
    }

    @Override
    protected void execute() {
        model.changeFactoryFamily();
    }
}
