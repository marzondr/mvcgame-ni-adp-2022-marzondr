package cz.cvut.fit.miadp.mvcgame.controller;

import cz.cvut.fit.miadp.mvcgame.command.*;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class GameController {

    private IGameModel model;
    private Timer timer;

    boolean isPressedE;
    boolean isPressedQ;
    boolean isPressedF;
    boolean isPressedT;
    boolean isPressedR;
    boolean isPressedU;
    boolean isPressedO;
    boolean isPressedSPACE;

    public GameController( IGameModel model ) {
        this.model = model;
        this.timer = new Timer();

        this.isPressedE = false;
        this.isPressedQ = false;
        this.isPressedF = false;
        this.isPressedT = false;
        this.isPressedR = false;
        this.isPressedU = false;
        this.isPressedO = false;
        this.isPressedSPACE = false;
    }

    public void processPressedKeys( List<String> pressedKeysCodes ) {

        for( String code : pressedKeysCodes ) {
            switch( code ) {
                case "UP":
                    this.model.registerCommand( new MoveCannonUpCommand(model) );
                    break;
                case "DOWN":
                    this.model.registerCommand( new MoveCannonDownCommand(model) );
                    break;
                case "LEFT":
                    this.model.registerCommand( new MoveCannonLeftCommand(model) );
                    break;
                case "RIGHT":
                    this.model.registerCommand( new MoveCannonRightCommand(model) );
                    break;

                case "W":
                    this.model.registerCommand( new AimCannonUpCommand(model) );
                    break;
                case "S":
                    this.model.registerCommand( new AimCannonDownCommand(model) );
                    break;
                case "D":
                    this.model.registerCommand( new PowerCannonUpCommand(model) );
                    break;
                case "A":
                    this.model.registerCommand( new PowerCannonDownCommand(model) );
                    break;

                case "Q":
                    if (!isPressedQ) {
                        this.model.registerCommand( new MissilesDecreasedCommand(model) );
                        isPressedQ = true;
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                isPressedQ = false;
                            }
                        }, 500);
                    }
                    break;
                case "E":
                    if (!isPressedE) {
                        this.model.registerCommand( new MissilesIncreasedCommand(model) );
                        isPressedE = true;
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                isPressedE = false;
                            }
                        }, 500);
                    }
                    break;

                case "F":
                    if (!isPressedF) {
                        this.model.registerCommand( new ChangeFamilyFactoryCommand(model) );
                        isPressedF = true;
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                isPressedF = false;
                            }
                        }, 500);
                    }
                    break;
                case "T":
                    if (!isPressedT) {
                        this.model.registerCommand( new ChangeMovingStrategyCommand(model) );
                        isPressedT = true;
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                isPressedT = false;
                            }
                        }, 500);
                    }
                    break;

                case "R":
                    if (!isPressedR) {
                        this.model.registerCommand( new ChangeShootingModeCommand(model) );
                        isPressedR = true;
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                isPressedR = false;
                            }
                        }, 500);
                    }
                    break;

                case "SPACE":
                    if (!isPressedSPACE) {
                        this.model.registerCommand( new ShootCommand(model) );
                        isPressedSPACE = true;
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                isPressedSPACE = false;
                            }
                        }, 400);
                    }
                    break;

                case "I":
                    if (!isPressedU) {
                        this.model.registerCommand( new UndoCommand(model) );
                        isPressedU = true;
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                isPressedU = false;
                            }
                        }, 100);
                    }
                    break;

                case "O":
                    if (!isPressedO) {
                        this.model.registerCommand( new RedoCommand(model) );
                        isPressedO = true;
                        timer.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                isPressedO = false;
                            }
                        }, 100);
                    }
                    break;

                default:
                    //nothing
            }
        }
    }
}
