package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.visitor.Visitable;
import cz.cvut.fit.miadp.mvcgame.visitor.Visitor;

public abstract class AbsGameInfo implements Visitable {

    protected String text;

    public abstract String getText();

    public abstract void setText(GameModel model);

    @Override
    public void acceptVisitor(Visitor visitor) {
        visitor.visitGameInfo( this );
    }
}
