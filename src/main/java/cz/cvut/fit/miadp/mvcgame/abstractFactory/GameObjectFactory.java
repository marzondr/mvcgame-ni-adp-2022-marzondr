package cz.cvut.fit.miadp.mvcgame.abstractFactory;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.SightPattern;

public interface GameObjectFactory {

    AbsCannon createCannon();
    AbsMissile createMissile(AbsCannon cannon);
    AbsMissile createMissile(Position position, double angle, int power);
    AbsCollision createCollision(Position position);
    AbsGameInfo createGameInfo();
    AbsSight createSight(Position position, SightPattern sightPattern);
}
