package cz.cvut.fit.miadp.mvcgame.abstractFactory;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.Sight;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.SightPattern;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB.*;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;
import cz.cvut.fit.miadp.mvcgame.strategy.RealisticMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;

public class GameObjectFactoryB implements GameObjectFactory {

    private IGameModel model;

    public GameObjectFactoryB(IGameModel gameModel) {
        this.model = gameModel;
    }

    @Override
    public AbsCannon createCannon() {
        return new CannonB( new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y), this );
    }

    @Override
    public AbsMissile createMissile( AbsCannon cannon ) {
        return new MissileB( new Position(cannon.getPosition().getX(), cannon.getPosition().getY()), cannon.getAngle(), cannon.getPower(), new RealisticMovingStrategy());
    }

    @Override
    public AbsMissile createMissile( Position position, double angle, int power ) {
        return new MissileB( new Position(position.getX(), position.getY()), angle, power, new SimpleMovingStrategy());
    }

    @Override
    public AbsCollision createCollision( Position position ) {
        return new CollisionB(new Position(position.getX(), position.getY()));
    }

    @Override
    public AbsGameInfo createGameInfo() {
        return new GameInfoB();
    }

    @Override
    public AbsSight createSight(Position position, SightPattern sightPattern) {
        return new Sight( new Position(position.getX(), position.getY()), sightPattern );
    }
}
