package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.strategy.MovingStrategy;

public class MissileB extends AbsMissile {

    private final MovingStrategy movingStrategy;

    public MissileB(Position position, double initAngle, int initVelocity, MovingStrategy movingStrategy) {
        super(position, initAngle, initVelocity);
        this.movingStrategy = movingStrategy;
    }

    @Override
    public void move() {
        this.movingStrategy.updatePosition(this);
    }
}
