package cz.cvut.fit.miadp.mvcgame.model;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectFactoryA;
import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectFactoryB;
import cz.cvut.fit.miadp.mvcgame.command.AbsCommand;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.iterator.HistoryIterator;
import cz.cvut.fit.miadp.mvcgame.iterator.UndoHistoryIterator;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.SightPattern;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.multiton.Enemy;
import cz.cvut.fit.miadp.mvcgame.observer.IObservable;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;
import cz.cvut.fit.miadp.mvcgame.strategy.*;
import cz.cvut.fit.miadp.mvcgame.visitor.SoundEffects;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public class GameModel implements IGameModel, IObservable {

    private int score = 0;
    private int scoreMultiplier = 0;

    private AbsCannon cannon;
    private AbsGameInfo gameInfo;
    private List<AbsMissile> missiles;
    private List<AbsCollision> collisions;

    private List<AbsSight> sights;
    private List<SightPattern> sightPatterns;

    private MovingStrategy movingStrategy;

    private List<IObserver> observers;

    private SoundEffects soundEffects;

    private GameObjectFactory gameObjectFactory;

    private Queue<AbsCommand> unExecutedCommands;
    private HistoryIterator it;

    public GameModel( ) {

        this.gameObjectFactory = new GameObjectFactoryA(this);

        this.cannon = this.gameObjectFactory.createCannon();
        this.gameInfo = this.gameObjectFactory.createGameInfo();

        this.missiles = new ArrayList<>();
        this.collisions = new ArrayList<>();

        this.sights = new ArrayList<>();
        this.sightPatterns = new ArrayList<>();
        this.sightPatterns.add( new SightPattern("sight.png") );
        for (int i = 0; i < MvcGameConfig.AIM_LENGHT; i++) {
            sights.add( gameObjectFactory.createSight(cannon.getPosition(), this.sightPatterns.get(0)));
        }

        this.movingStrategy = new SimpleMovingStrategy();

        this.observers = new ArrayList<>();

        this.soundEffects = new SoundEffects();

        this.unExecutedCommands = new LinkedBlockingQueue<>();
        this.it = new UndoHistoryIterator();
    }

    @Override
    public void registerCommand(AbsCommand cmd) {
        unExecutedCommands.add(cmd);
    }

    @Override
    public void redoLastCommand() {
        it.forward();
        this.notifyObservers();
    }

    @Override
    public void undoLastCommand() {
        it.back();
        this.notifyObservers();
    }

    public void update( ) {

        this.gameInfo.setText(this);

        this.executeCommands();
        this.moveSights();
        this.moveMissiles();
        this.checkCollisions();
        this.destroyCollisions();

        Enemy.checkNumOfAlive();
    }

    private void executeCommands() {
        while( !this.unExecutedCommands.isEmpty() ) {
            AbsCommand cmd = unExecutedCommands.remove();
            cmd.doExecute();
            it.add( cmd );
        }
    }

    private void moveSights() {
        AbsMissile traceMissile = gameObjectFactory.createMissile(this.cannon.getPosition(), this.cannon.getAngle(), this.cannon.getPower());
        traceMissile.move();
        for (AbsSight sight: this.sights) {
            traceMissile.move();
            traceMissile.move();
            sight.getPosition().setX( traceMissile.getPosition().getX() );
            sight.getPosition().setY( traceMissile.getPosition().getY() );
        }
    }

    private void moveMissiles() {

        for (AbsMissile missile: this.missiles) {
            missile.move();
        }

        this.destroyMissiles();
        this.notifyObservers();
    }

    private void destroyMissiles() {
        List<AbsMissile> toRemove = new ArrayList<>();

        for (AbsMissile missile: this.missiles) {
            if (missile.getPosition().getX() > MvcGameConfig.MAX_X ||
                    missile.getPosition().getY() > MvcGameConfig.MAX_Y ||
                        missile.getAge() > MvcGameConfig.MAX_MIS_AGE) {

                if (missile.getHits() == 0) {
                    this.scoreMultiplier = 0;
                    this.getCannon().setSingleShootingMode();
                }
                else {
                    this.scoreMultiplier++;
                    if (this.scoreMultiplier >= 5)
                        this.getCannon().setDoubleShootingMode();
                }

                toRemove.add(missile);
            }
        }
        this.missiles.removeAll(toRemove);
    }

    private void checkCollisions() {
        for (AbsMissile missile: this.missiles) {
            for (Enemy enemy: Enemy.getInstances()) {
                if (missile.getPosition().intersects(enemy.getPosition())) {
                    enemy.acceptVisitor( this.soundEffects );
                    enemy.die();
                    this.score += 100 + missile.getHits() * 100;
                    if (this.gameObjectFactory instanceof GameObjectFactoryA && this.score >= 10000)
                        this.changeFactoryFamily();
                    missile.hit();
                    this.collisions.add(
                            gameObjectFactory.createCollision(
                                    enemy.getPosition()
                            )
                    );
                }
            }
        }
    }

    private void destroyCollisions() {
        List<AbsCollision> toRemove = new ArrayList<>();

        for (AbsCollision collision: this.collisions) {
            if (collision.getAge() >= 500)
                toRemove.add(collision);
        }

        this.collisions.removeAll(toRemove);
    }

    public void cannonShoot( ) {
        this.cannon.acceptVisitor( this.soundEffects );
        this.missiles.addAll(cannon.shoot());
        this.notifyObservers();
    }

    public void moveCannonUp( ) {
        this.cannon.moveUp( );
        this.notifyObservers();
    }

    public void moveCannonDown( ) {
        this.cannon.moveDown( );
        this.notifyObservers();
    }

    public void moveCannonLeft( ) {
        this.cannon.moveLeft( );
        this.notifyObservers();
    }

    public void moveCannonRight( ) {
        this.cannon.moveRight( );
        this.notifyObservers();
    }

    public void changeFactoryFamily() {
        if (gameObjectFactory instanceof GameObjectFactoryA)
            gameObjectFactory = new GameObjectFactoryB(this);
        else
            gameObjectFactory = new GameObjectFactoryA(this);

        AbsCannon oldCannon = this.cannon;
        this.cannon = gameObjectFactory.createCannon();
        this.cannon.getPosition().setX( oldCannon.getPosition().getX() );
        this.cannon.getPosition().setY( oldCannon.getPosition().getY() );
        if (gameObjectFactory instanceof GameObjectFactoryA)
            this.cannon.setAngle(0);
        else
            this.cannon.setAngle( oldCannon.getAngle() );
        this.cannon.setPower( oldCannon.getPower() );
    }

    public void changeMovingStrategy() {
        if (movingStrategy instanceof SimpleMovingStrategy)
            movingStrategy = new RealisticMovingStrategy();
        else if (movingStrategy instanceof RealisticMovingStrategy)
            movingStrategy = new GoniometricMovingStrategy();
        else
            movingStrategy = new SimpleMovingStrategy();
    }

    @Override
    public void registerObserver( IObserver obs ) {
        if( !this.observers.contains( obs ) ) {
            this.observers.add( obs );
        }
    }

    @Override
    public void unregisterObserver( IObserver obs ) {
        if( this.observers.contains( obs ) ) {
            this.observers.remove( obs );
        }
    }

    @Override
    public void notifyObservers( ) {
        for( IObserver obs : this.observers ){
            obs.update( );
        }
    }

    public int getScore() {
        return score;
    }

    public List<GameObject> getGameObjects() {
        List<GameObject> gameObjects = new ArrayList<>();
        gameObjects.add( this.cannon );
        gameObjects.addAll( Enemy.getInstances() );
        gameObjects.addAll( this.missiles );
        gameObjects.addAll( this.collisions );
        gameObjects.addAll( this.sights );
        return gameObjects;
    }

    public List<Enemy> getEnemies() {
        return Enemy.getInstances().stream().toList();
    }

    public List<AbsMissile> getMissiles() {
        return missiles;
    }

    public List<AbsCollision> getCollisions() { return collisions; }

    public MovingStrategy getMovingStrategy() {
        return this.movingStrategy;
    }

    public AbsCannon getCannon() {
        return this.cannon;
    }

    public AbsGameInfo getGameInfo() { return gameInfo; }

    public void aimCannonUp() {
        this.cannon.aimUp();
        notifyObservers();
    }

    public void aimCannonDown() {
        this.cannon.aimDown();
        notifyObservers();
    }

    public void cannonPowerUp() {
        this.cannon.powerUp();
        notifyObservers();
    }

    public void cannonPowerDown() {
        this.cannon.powerDown();
        notifyObservers();
    }

    public void toggleCannonShootingMode() {
        this.cannon.toggleShootingMode();
        notifyObservers();
    }

    public void cannonMissilesIncrease() {
        this.cannon.increaseMissiles();
        notifyObservers();
    }

    public void cannonMissilesDecrease() {
        this.cannon.decreaseMissiles();
        notifyObservers();
    }

    private static class Memento {
        private final int score;

        private final int cannon_X;
        private final int cannon_Y;
        private final double angle;
        private final int power;
        private final int shootingMode;
        private final int movingStrategy;
        private final int gameObjectFactory;

        private ArrayList<Integer> enemy_X = new ArrayList<>();
        private ArrayList<Integer> enemy_Y = new ArrayList<>();

        public Memento(int score, AbsCannon cannon, MovingStrategy movingStrategy, GameObjectFactory gameObjectFactory) {
            this.score = score;

            this.cannon_X = cannon.getPosition().getX();
            this.cannon_Y = cannon.getPosition().getY();
            this.angle = cannon.getAngle();
            this.power = cannon.getPower();
            switch (cannon.getShootingMode().getName()) {
                case "Double" -> this.shootingMode = 1;
                case "Dynamic" -> this.shootingMode = 2;
                default -> this.shootingMode = 0;
            }
            switch (movingStrategy.getName()) {
                case "Realistic" -> this.movingStrategy = 1;
                case "Goniometric" -> this.movingStrategy = 2;
                default -> this.movingStrategy = 0;
            }
            this.gameObjectFactory = gameObjectFactory instanceof GameObjectFactoryA? 0 : 1;
            for (AbsEnemy enemy: Enemy.getInstances()) {
                enemy_X.add(enemy.getPosition().getX());
                enemy_Y.add(enemy.getPosition().getY());
            }
        }
    }

    public void setMemento(Object memento) {
        Memento m = ( Memento ) memento;

        this.gameObjectFactory = m.gameObjectFactory == 0 ? new GameObjectFactoryA(this) : new GameObjectFactoryB(this);
        this.score = m.score;

        this.cannon = this.gameObjectFactory.createCannon();
        this.cannon.getPosition().setX( m.cannon_X );
        this.cannon.getPosition().setY( m.cannon_Y );
        this.cannon.setPower( m.power );
        this.cannon.setAngle( m.angle );
        for (int i = 0; i < m.shootingMode; i++)
            this.cannon.toggleShootingMode();

        for (int i = 0; i < m.movingStrategy; i++)
            this.changeMovingStrategy();

        for (int i = 0; i < m.enemy_X.size(); i++)
            Enemy.getNthAliveInstance(i+1).getPosition().setX( m.enemy_X.get(i) );

        for (int i = 0; i < m.enemy_Y.size(); i++)
            Enemy.getNthAliveInstance(i+1).getPosition().setY( m.enemy_Y.get(i) );

        notifyObservers();
    }

    public Object createMemento() {
        return new Memento(score, cannon, movingStrategy, gameObjectFactory);
    }
}
