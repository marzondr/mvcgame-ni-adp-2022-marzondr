package cz.cvut.fit.miadp.mvcgame.proxy;

import cz.cvut.fit.miadp.mvcgame.command.AbsCommand;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.multiton.Enemy;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.strategy.MovingStrategy;

import java.util.List;

public class GameModelProxy implements IGameModel {

    private final IGameModel model;
    private String rights;

    public GameModelProxy(IGameModel model) {
        this.model = model;
        this.rights = "basic";
    }

    @Override
    public void registerCommand(AbsCommand cmd) {
        model.registerCommand(cmd);
    }

    @Override
    public void redoLastCommand() {
        model.redoLastCommand();
    }

    @Override
    public void undoLastCommand() {
        model.undoLastCommand();
    }

    @Override
    public void update() {
        model.update();
    }

    @Override
    public void cannonShoot() {
        model.cannonShoot();
    }

    @Override
    public void moveCannonUp() {
        model.moveCannonUp();
    }

    @Override
    public void moveCannonDown() {
        model.moveCannonDown();
    }

    @Override
    public void moveCannonLeft() {
        model.moveCannonLeft();
    }

    @Override
    public void moveCannonRight() {
        model.moveCannonRight();
    }

    @Override
    public void changeFactoryFamily() {
        model.changeFactoryFamily();
    }

    @Override
    public void changeMovingStrategy() {
        model.changeMovingStrategy();
    }

    @Override
    public void registerObserver(IObserver obs) {
        model.registerObserver(obs);
    }

    @Override
    public void unregisterObserver(IObserver obs) {
        model.unregisterObserver(obs);
    }

    @Override
    public void notifyObservers() {
        model.notifyObservers();
    }

    @Override
    public int getScore() {
        return model.getScore();
    }

    @Override
    public List<GameObject> getGameObjects() {
        return model.getGameObjects();
    }

    @Override
    public List<Enemy> getEnemies() {
        return model.getEnemies();
    }

    @Override
    public List<AbsMissile> getMissiles() {
        return model.getMissiles();
    }

    @Override
    public List<AbsCollision> getCollisions() {
        return model.getCollisions();
    }

    @Override
    public MovingStrategy getMovingStrategy() {
        return model.getMovingStrategy();
    }

    @Override
    public AbsCannon getCannon() {
        return model.getCannon();
    }

    @Override
    public AbsGameInfo getGameInfo() {
        return model.getGameInfo();
    }

    @Override
    public void aimCannonUp() {
        model.aimCannonUp();
    }

    @Override
    public void aimCannonDown() {
        model.aimCannonDown();
    }

    @Override
    public void cannonPowerUp() {
        model.cannonPowerUp();
    }

    @Override
    public void cannonPowerDown() {
        model.cannonPowerDown();
    }

    @Override
    public void toggleCannonShootingMode() {
        model.toggleCannonShootingMode();
    }

    @Override
    public void cannonMissilesIncrease() {
        model.cannonMissilesIncrease();
    }

    @Override
    public void cannonMissilesDecrease() {
        model.cannonMissilesDecrease();
    }

    @Override
    public void setMemento(Object memento) {
        model.setMemento(memento);
    }

    @Override
    public Object createMemento() {
        return model.createMemento();
    }
}
