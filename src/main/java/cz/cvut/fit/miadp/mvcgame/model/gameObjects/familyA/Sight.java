package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsSight;

public class Sight extends AbsSight {

    SightPattern sightPattern;

    public Sight(Position position, SightPattern sightPattern) {
        this.position = position;
        this.sightPattern = sightPattern;
    }

    @Override
    public String getSightPatternImage() {
        return sightPattern.getImageName();
    }
}
