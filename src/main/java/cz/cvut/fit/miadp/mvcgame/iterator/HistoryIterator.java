package cz.cvut.fit.miadp.mvcgame.iterator;

import cz.cvut.fit.miadp.mvcgame.command.AbsCommand;

public interface HistoryIterator {

    public void back();
    public void forward();
    public void add(AbsCommand cmd);
}
