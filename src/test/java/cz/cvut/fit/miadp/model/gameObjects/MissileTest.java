package cz.cvut.fit.miadp.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.MissileA;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB.MissileB;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;
import org.junit.Assert;
import org.junit.Test;

public class MissileTest {

    private static final double DELTA = 1e-15;

    @Test
    public void initialisationTest() {
        Position position = new Position(0, 0);

        AbsMissile missileA = new MissileA( new Position(0, 0), 0, 10, new SimpleMovingStrategy());
        AbsMissile missileB = new MissileB( new Position(0, 0), 0, 10, new SimpleMovingStrategy());

        Assert.assertEquals(missileA.getPosition().getX(), position.getX());
        Assert.assertEquals(missileA.getPosition().getY(), position.getY());
        Assert.assertEquals(missileB.getPosition().getX(), position.getX());
        Assert.assertEquals(missileB.getPosition().getY(), position.getY());

        Assert.assertEquals(missileA.getInitAngle(), 0, DELTA);
        Assert.assertEquals(missileA.getInitVelocity(), 1);
        Assert.assertEquals(missileB.getInitAngle(), 0, DELTA);
        Assert.assertEquals(missileB.getInitVelocity(), 1);
    }

    @Test
    public void moveTest() {
        int X = ( int )( Math.cos( 0 ) );
        int Y = ( int )( Math.sin( 0 ) );

        Position position = new Position(0, 0);
        position.add( new Vector(X, Y) );

        AbsMissile missileA = new MissileA( new Position(0, 0), 0, 10, new SimpleMovingStrategy());
        AbsMissile missileB = new MissileB( new Position(0, 0), 0, 10, new SimpleMovingStrategy());

        missileA.move();
        missileB.move();

        Assert.assertEquals(missileA.getPosition().getX(), position.getX());
        Assert.assertEquals(missileA.getPosition().getY(), position.getY());
        Assert.assertEquals(missileB.getPosition().getX(), position.getX());
        Assert.assertEquals(missileB.getPosition().getY(), position.getY());
    }

    @Test
    public void moveInAngleTest() {
        int X = ( int )( Math.cos( 2 ) );
        int Y = ( int )( Math.sin( 2 ) );

        Position position = new Position(0, 0);
        position.add( new Vector(X, Y) );

        AbsMissile missileA = new MissileA( new Position(0, 0), 2, 10, new SimpleMovingStrategy());
        AbsMissile missileB = new MissileB( new Position(0, 0), 2, 10, new SimpleMovingStrategy());

        missileA.move();
        missileB.move();

        Assert.assertEquals(missileA.getPosition().getX(), position.getX());
        Assert.assertEquals(missileA.getPosition().getY(), position.getY());
        Assert.assertEquals(missileB.getPosition().getX(), position.getX());
        Assert.assertEquals(missileB.getPosition().getY(), position.getY());
    }
}
