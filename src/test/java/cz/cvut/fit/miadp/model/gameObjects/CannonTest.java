package cz.cvut.fit.miadp.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectFactoryA;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.CannonA;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.MissileA;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB.CannonB;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CannonTest {

    private static final double DELTA = 1e-15;

    @Test
    public void initialisationTest() {
        GameObjectFactory objectFactory = mock( GameObjectFactoryA.class );
        Position position = new Position(0, 0);

        AbsCannon cannonA = new CannonA( position, objectFactory );
        AbsCannon cannonB = new CannonB( position, objectFactory );

        Assert.assertEquals(cannonA.getPosition().getX(), position.getX());
        Assert.assertEquals(cannonA.getPosition().getY(), position.getY());
        Assert.assertEquals(cannonB.getPosition().getX(), position.getX());
        Assert.assertEquals(cannonB.getPosition().getY(), position.getY());

        Assert.assertEquals(cannonA.getPower(), MvcGameConfig.INIT_POWER);
        Assert.assertEquals(cannonA.getAngle(), MvcGameConfig.INIT_ANGLE, DELTA);
        Assert.assertEquals(cannonB.getPower(), MvcGameConfig.INIT_POWER * 2);
        Assert.assertEquals(cannonB.getAngle(), MvcGameConfig.INIT_ANGLE, DELTA);

        Assert.assertNotNull(cannonA.getShootingMode());
        Assert.assertNotNull(cannonA.getShootingBatch());
        Assert.assertNotNull(cannonB.getShootingMode());
        Assert.assertNotNull(cannonB.getShootingBatch());

        Mockito.verify(objectFactory, never()).createCannon();
        Mockito.verify(objectFactory, never()).createGameInfo();
        Mockito.verify(objectFactory, never()).createMissile(any());
        Mockito.verify(objectFactory, never()).createCollision(any());
    }

    @Test
    public void primitiveShootTest() {
        GameObjectFactory objectFactory = mock( GameObjectFactoryA.class );
        Position position = new Position(0, 0);
        AbsCannon cannon = new CannonA( position, objectFactory );

        when( objectFactory.createMissile(cannon) ).thenReturn(new MissileA( new Position(position.getX(), position.getY()),
                                                                                MvcGameConfig.INIT_ANGLE,
                                                                                MvcGameConfig.INIT_POWER,
                                                                                new SimpleMovingStrategy()));
        Assert.assertEquals(cannon.getShootingBatch().size(), 0);

        cannon.primitiveShoot();

        verify(objectFactory, times(1)).createMissile(cannon);
        Assert.assertEquals(cannon.getShootingBatch().size(), 1);
    }

    @Test
    public void moveDownTest() {
        GameObjectFactory objectFactory = mock( GameObjectFactoryA.class );
        Position position = new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y);
        AbsCannon cannonA = new CannonA( new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y), objectFactory );
        AbsCannon cannonB = new CannonB( new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y), objectFactory );

        cannonA.moveDown();
        cannonB.moveDown();

        position.add( new Vector(0, MvcGameConfig.MOVE_STEP));

        Assert.assertEquals( cannonA.getPosition().getX(), position.getX() );
        Assert.assertEquals( cannonA.getPosition().getY(), position.getY() );
        Assert.assertEquals( cannonB.getPosition().getX(), position.getX() );
        Assert.assertEquals( cannonB.getPosition().getY(), position.getY() );
    }

    @Test
    public void moveUpTest() {
        GameObjectFactory objectFactory = mock( GameObjectFactoryA.class );
        Position position = new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y);
        AbsCannon cannonA = new CannonA( new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y), objectFactory );
        AbsCannon cannonB = new CannonB( new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y), objectFactory );

        for (int i = 0; i < ((MvcGameConfig.CANNON_POS_Y)+60)/MvcGameConfig.MOVE_STEP; i++) {
            cannonA.moveUp();
            cannonB.moveUp();

            position.add(new Vector(0, -1 * MvcGameConfig.MOVE_STEP));
        }

        Assert.assertEquals( cannonA.getPosition().getX(), position.getX() );
        Assert.assertEquals( cannonA.getPosition().getY(), position.getY() );
        Assert.assertEquals( cannonB.getPosition().getX(), position.getX() );
        Assert.assertEquals( cannonB.getPosition().getY(), position.getY() );

        cannonA.moveUp();
        cannonB.moveUp();

        position.add(new Vector(0, -1 * MvcGameConfig.MOVE_STEP));

        Assert.assertEquals( cannonA.getPosition().getX(), position.getX() );
        Assert.assertNotEquals( cannonA.getPosition().getY(), position.getY() ); // CannonA cannot get off-screen
        Assert.assertEquals( cannonB.getPosition().getX(), position.getX() );
        Assert.assertEquals( cannonB.getPosition().getY(), position.getY() );
    }

    @Test
    public void movingTest() {
        GameObjectFactory objectFactory = mock( GameObjectFactoryA.class );
        AbsCannon cannonA = new CannonA( new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y), objectFactory );
        AbsCannon cannonB = new CannonB( new Position(MvcGameConfig.CANNON_POS_X, MvcGameConfig.CANNON_POS_Y), objectFactory );

        cannonA.moveRight();
        cannonB.moveRight();

        Assert.assertNotEquals(cannonA.getPosition().getX(), cannonB.getPosition().getX());
        Assert.assertEquals(cannonA.getPosition().getY(), cannonB.getPosition().getY());

        cannonB.moveLeft();

        Assert.assertEquals(cannonA.getPosition().getY(), cannonB.getPosition().getY());
        Assert.assertEquals(cannonA.getPosition().getX(), cannonB.getPosition().getX());
    }
}
