package cz.cvut.fit.miadp.memento;


import cz.cvut.fit.miadp.mvcgame.command.AbsCommand;
import cz.cvut.fit.miadp.mvcgame.command.MoveCannonDownCommand;
import cz.cvut.fit.miadp.mvcgame.iterator.HistoryIterator;
import cz.cvut.fit.miadp.mvcgame.iterator.UndoHistoryIterator;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class IteratorTest {

    @Test
    public void BackTest() {
        HistoryIterator iterator = new UndoHistoryIterator();
        AbsCommand cmd = mock( MoveCannonDownCommand.class );

        iterator.add(cmd);
        iterator.back();

        verify(cmd, times(1)).unExecute();
    }

    @Test
    public void ForwardTest() {
        HistoryIterator iterator = new UndoHistoryIterator();
        AbsCommand cmd = mock( MoveCannonDownCommand.class );

        iterator.add(cmd);
        iterator.forward();
        verify(cmd, times(0)).unExecute();

        iterator.back();
        iterator.forward();
        verify(cmd, times(2)).unExecute();
    }

    @Test
    public void AddTest() {
        HistoryIterator iterator = new UndoHistoryIterator();
        AbsCommand cmd1 = mock( MoveCannonDownCommand.class );
        AbsCommand cmd2 = mock( MoveCannonDownCommand.class );
        AbsCommand cmd3 = mock( MoveCannonDownCommand.class );

        iterator.add(cmd1);
        iterator.add(cmd2);
        iterator.back();
        verify(cmd1, times(0)).unExecute();
        verify(cmd2, times(1)).unExecute();
        iterator.add(cmd3);
        iterator.back();
        verify(cmd3, times(1)).unExecute();
        iterator.back();
        verify(cmd1, times(1)).unExecute();
    }
}
