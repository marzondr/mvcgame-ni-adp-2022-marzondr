package cz.cvut.fit.miadp.memento;

import cz.cvut.fit.miadp.mvcgame.memento.Caretaker;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class CaretakerTest {

    IGameModel model = mock( GameModel.class );

    @Test
    public void createMementoFailTest() {
        Assert.assertNull(Caretaker.getInstance().createMemento());
        verify(model, never()).createMemento();
    }

    @Test
    public void setMementoFailTest() {
        verify(model, never()).setMemento(any());
    }

    @Test
    public void createMementoTest() {
        Object rtrn = new Object();
        when(model.createMemento()).thenReturn(rtrn);

                      Caretaker.getInstance().setModel(model);
        Object copy = Caretaker.getInstance().createMemento();

        Assert.assertEquals(copy, rtrn);
    }

    @Test
    public void seMementoTest() {
        Object rtrn = new Object();

        Caretaker.getInstance().setModel(model);
        Caretaker.getInstance().setMemento(rtrn);

        verify(model, times(1)).setMemento(rtrn);
    }
}
