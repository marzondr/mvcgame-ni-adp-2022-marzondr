package cz.cvut.fit.miadp.memento;

import cz.cvut.fit.miadp.mvcgame.command.ChangeShootingModeCommand;
import cz.cvut.fit.miadp.mvcgame.command.MoveCannonDownCommand;
import cz.cvut.fit.miadp.mvcgame.command.MoveCannonUpCommand;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.memento.Caretaker;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;
import org.junit.Assert;
import org.junit.Test;

public class UndoCommandTest {

    @Test
    public void undoChangeShootingModeCommandTest( ){
        IGameModel model = new GameModel( );
        Caretaker.getInstance().setModel(model);
        model.registerCommand( new MoveCannonDownCommand( model ) );
        model.update();

        int shootingModeNumStart;
        int shootingModeNumAfter;
        int shootingModeNumUndo;

        switch (model.getCannon().getShootingMode().getName()) {
            case "Double" -> shootingModeNumStart = 1;
            case "Dynamic" -> shootingModeNumStart = 2;
            default -> shootingModeNumStart = 0;
        }

        model.registerCommand( new ChangeShootingModeCommand( model ) );
        model.update( );

        switch (model.getCannon().getShootingMode().getName()) {
            case "Double" -> shootingModeNumAfter = 1;
            case "Dynamic" -> shootingModeNumAfter = 2;
            default -> shootingModeNumAfter = 0;
        }

        model.undoLastCommand();

        switch (model.getCannon().getShootingMode().getName()) {
            case "Double" -> shootingModeNumUndo = 1;
            case "Dynamic" -> shootingModeNumUndo = 2;
            default -> shootingModeNumUndo = 0;
        }

        Assert.assertEquals( shootingModeNumStart, shootingModeNumAfter - 1 );
        Assert.assertEquals( shootingModeNumStart, shootingModeNumUndo );
    }

    @Test
    public void undoCommandTest( ){
        IGameModel model = new GameModel( );
        Caretaker.getInstance().setModel(model);

        int positionBeforeUndoX = model.getCannon( ).getPosition().getX( );
        int positionBeforeUndoY = model.getCannon( ).getPosition( ).getY( );

        model.registerCommand( new MoveCannonUpCommand( model ) );
        model.update( );

        int positionAfterExcecution = model.getCannon( ).getPosition( ).getY( );

        model.undoLastCommand( );

        int positionAfterUndoX = model.getCannon( ).getPosition( ).getX( );
        int positionAfterUndoY = model.getCannon( ).getPosition( ).getY( );

        Assert.assertEquals( positionBeforeUndoY, positionAfterExcecution + MvcGameConfig.MOVE_STEP );
        Assert.assertEquals( positionBeforeUndoX, positionAfterUndoX );
        Assert.assertEquals( positionBeforeUndoY, positionAfterUndoY );
    }
}
