package cz.cvut.fit.miadp.memento;

import cz.cvut.fit.miadp.mvcgame.command.ChangeShootingModeCommand;
import cz.cvut.fit.miadp.mvcgame.memento.Caretaker;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;
import org.junit.Assert;
import org.junit.Test;

public class RedoCommandTest {

    @Test
    public void undoChangeShootingModeCommandTest( ){
        IGameModel model = new GameModel( );
        Caretaker.getInstance().setModel(model);
        model.registerCommand( new ChangeShootingModeCommand( model ) );
        model.update();

        int shootingModeNumStart;
        int shootingModeNumAfter;
        int shootingModeNumUndo;
        int shootingModeNumRedo;

        switch (model.getCannon().getShootingMode().getName()) {
            case "Double" -> shootingModeNumStart = 1;
            case "Dynamic" -> shootingModeNumStart = 2;
            default -> shootingModeNumStart = 0;
        }

        model.registerCommand( new ChangeShootingModeCommand( model ) );
        model.update( );

        switch (model.getCannon().getShootingMode().getName()) {
            case "Double" -> shootingModeNumAfter = 1;
            case "Dynamic" -> shootingModeNumAfter = 2;
            default -> shootingModeNumAfter = 0;
        }

        model.undoLastCommand();

        switch (model.getCannon().getShootingMode().getName()) {
            case "Double" -> shootingModeNumUndo = 1;
            case "Dynamic" -> shootingModeNumUndo = 2;
            default -> shootingModeNumUndo = 0;
        }

        Assert.assertEquals( shootingModeNumStart, shootingModeNumAfter - 1 );
        Assert.assertEquals( shootingModeNumStart, shootingModeNumUndo );

        model.redoLastCommand();
        model.redoLastCommand();

        switch (model.getCannon().getShootingMode().getName()) {
            case "Double" -> shootingModeNumRedo = 1;
            case "Dynamic" -> shootingModeNumRedo = 2;
            default -> shootingModeNumRedo = 0;
        }

        Assert.assertEquals( shootingModeNumRedo, shootingModeNumStart );
        Assert.assertEquals( shootingModeNumRedo, shootingModeNumAfter - 1 );
    }
}
