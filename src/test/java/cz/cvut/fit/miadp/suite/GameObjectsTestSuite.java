package cz.cvut.fit.miadp.suite;

import cz.cvut.fit.miadp.model.gameObjects.CannonTest;
import cz.cvut.fit.miadp.model.gameObjects.MissileTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith( Suite.class )

@Suite.SuiteClasses(
        {
                CannonTest.class,
                MissileTest.class
        }
)

public class GameObjectsTestSuite {
}
