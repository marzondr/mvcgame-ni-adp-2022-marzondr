package cz.cvut.fit.miadp.suite;

import cz.cvut.fit.miadp.memento.IteratorTest;
import cz.cvut.fit.miadp.memento.RedoCommandTest;
import cz.cvut.fit.miadp.memento.UndoCommandTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith( Suite.class )
@Suite.SuiteClasses(
        {
                UndoCommandTest.class,
                RedoCommandTest.class,
                IteratorTest.class
        }
)

public class HistoryIterationSuite {
}
