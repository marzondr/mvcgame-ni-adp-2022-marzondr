package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.memento.CaretakerTest;
import cz.cvut.fit.miadp.suite.GameObjectsTestSuite;
import cz.cvut.fit.miadp.suite.HistoryIterationSuite;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class Test {

    public static void main(String[] args) {
        Result result;

        result = JUnitCore.runClasses(HistoryIterationSuite.class);

        for (Failure failure: result.getFailures())
            System.out.print(failure.toString());

        System.out.print(result.wasSuccessful());
        System.out.print('\n');

        result = JUnitCore.runClasses(GameObjectsTestSuite.class);

        for (Failure failure: result.getFailures())
            System.out.print(failure.toString());

        System.out.print(result.wasSuccessful());
        System.out.print('\n');

        result = JUnitCore.runClasses(CaretakerTest.class);

        for (Failure failure: result.getFailures())
            System.out.print(failure.toString());

        System.out.print(result.wasSuccessful());
        System.out.print('\n');
    }
}
